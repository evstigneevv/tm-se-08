package ru.evstigneev.tm.command.task;

import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.enumerated.RoleType;

public class TaskGetAllCommand extends AbstractCommand {

    @Override
    public String command() {
        return "SAT";
    }

    @Override
    public String description() {
        return "Show all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("|=====================================|=====================================|=====================================|");
        System.out.println("|          project ID                 |            task ID                  |               task name             |");
        System.out.println("|_____________________________________|_____________________________________|_____________________________________|");
        for (Task t : bootstrap.getTaskService().findAll()) {
            System.out.println("|" + t.getProjectId() + " |" + t.getId() + " | " + t.getName());
            System.out.println("|_____________________________________|_____________________________________|_____________________________________|");
        }
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN};
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}
