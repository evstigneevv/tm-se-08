package ru.evstigneev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.enumerated.RoleType;

public class TaskGetOneCommand extends AbstractCommand {

    @Override
    public String command() {
        return "SOT";
    }

    @Override
    public String description() {
        return "Shows specify task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("input user ID: ");
        @NotNull final String userId = bootstrap.getScanner().nextLine();
        System.out.println("input task ID: ");
        @NotNull final String taskId = bootstrap.getScanner().nextLine();
        if (bootstrap.getTaskService().findOne(userId, taskId) == null) {
            return;
        }
        @NotNull final Task t = bootstrap.getTaskService().findOne(userId, taskId);
        System.out.println("|=====================================|=====================================|=====================================|");
        System.out.println("|          project ID                 |            task ID                  |               task name             |");
        System.out.println("|_____________________________________|_____________________________________|_____________________________________|");
        System.out.println("|" + t.getProjectId() + " |" + t.getId() + " | " + t.getName());
        System.out.println("|_____________________________________|_____________________________________|_____________________________________|");
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN, RoleType.USER};
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}
