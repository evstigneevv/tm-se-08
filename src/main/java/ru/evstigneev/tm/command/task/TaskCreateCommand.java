package ru.evstigneev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.NoPermissionException;

public class TaskCreateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "CT";
    }

    @Override
    public String description() {
        return "Create new task";
    }

    @Override
    public boolean havePermission(@NotNull final String first, @NotNull final String second) throws Exception {
        boolean isAdmin = bootstrap.getUserService().isAdmin(bootstrap.getCurrentUser());
        boolean isPermittedUser = !bootstrap.getCurrentUser().getLogin()
                .equals(bootstrap.getProjectService().findOne(first, second).getUserId());
        if (!isAdmin && !isPermittedUser) {
            throw new NoPermissionException();
        }
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("input user ID: ");
        @NotNull final String userId = bootstrap.getScanner().nextLine();
        System.out.println("input project ID: ");
        @NotNull final String projectId = bootstrap.getScanner().nextLine();
        havePermission(userId, projectId);
        if (bootstrap.getProjectService().findOne(userId, projectId) != null) {
            System.out.println("input new task name into project: ");
            bootstrap.getTaskService().create(userId, projectId, bootstrap.getScanner().nextLine());
            System.out.println("task created!");
        } else {
            System.out.println("Project doesn't exist!");
        }
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN, RoleType.USER};
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}
