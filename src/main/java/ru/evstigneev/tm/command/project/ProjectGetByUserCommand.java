package ru.evstigneev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.enumerated.RoleType;

public class ProjectGetByUserCommand extends AbstractCommand {

    @Override
    public String command() {
        return "SUP";
    }

    @Override
    public String description() {
        return "Show all projects of specify user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Enter user ID: ");
        @NotNull final String input = bootstrap.getScanner().nextLine();
        System.out.println("|=====================================|=====================================|=====================================|");
        System.out.println("|          project ID                 |            project name             |            user ID                  |");
        System.out.println("|_____________________________________|_____________________________________|_____________________________________|");
        for (Project p : bootstrap.getProjectService().findAllByUserId(input)) {
            System.out.println("|" + p.getId() + " | " + p.getName());
            System.out.println("|_____________________________________|_____________________________________|_____________________________________|");
        }
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN, RoleType.USER};
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}
