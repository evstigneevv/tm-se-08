package ru.evstigneev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.NoPermissionException;

public class ProjectUpdateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "UP";
    }

    @Override
    public String description() {
        return "Update project";
    }

    @Override
    public boolean havePermission(@NotNull final String first, @NotNull final String second) throws Exception {
        boolean isAdmin = bootstrap.getUserService().isAdmin(bootstrap.getCurrentUser());
        boolean isPermittedUser = !bootstrap.getCurrentUser().getLogin()
                .equals(bootstrap.getProjectService().findOne(first, second).getUserId());
        if (!isAdmin && !isPermittedUser) {
            throw new NoPermissionException();
        }
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("input user ID");
        @NotNull final String userId = bootstrap.getScanner().nextLine();
        System.out.println("input project ID");
        @NotNull final String projectId = bootstrap.getScanner().nextLine();
        havePermission(userId, projectId);
        System.out.println(bootstrap.getCurrentUser().getLogin());
        System.out.println("input project new name");
        bootstrap.getProjectService().update(userId, projectId, bootstrap.getScanner().nextLine());
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN, RoleType.USER};
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}
