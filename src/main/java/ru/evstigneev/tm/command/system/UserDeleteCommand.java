package ru.evstigneev.tm.command.system;

import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.enumerated.RoleType;

public class UserDeleteCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DU";
    }

    @Override
    public String description() {
        return "Delete user by ID";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Enter user ID: ");
        if (bootstrap.getUserService().remove(bootstrap.getScanner().nextLine())) {
            System.out.println("User deleted!");
        } else {
            System.out.println("User wasn't deleted!");
        }
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN};
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}
