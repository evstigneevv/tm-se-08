package ru.evstigneev.tm.command.system;

import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.enumerated.RoleType;

public class UserLogOutCommand extends AbstractCommand {

    @Override
    public String command() {
        return "LO";
    }

    @Override
    public String description() {
        return "Log out current user";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.setCurrentUser(null);
        System.out.println("You log out successfully!");
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN, RoleType.USER};
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}
