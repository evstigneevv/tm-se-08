package ru.evstigneev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.enumerated.RoleType;

public class UserCreateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "CU";
    }

    @Override
    public String description() {
        return "Creates new user";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getUserService().findAll().isEmpty()) {
            System.out.println("Enter new user login: ");
            @NotNull final String login = bootstrap.getScanner().nextLine();
            System.out.println("Enter new user password: ");
            @NotNull final String password = bootstrap.getScanner().nextLine();
            bootstrap.getUserService().create(login, password);
            System.out.println("User created!");
            return;
        }
        System.out.println("Enter new user login: ");
        @NotNull final String login = bootstrap.getScanner().nextLine();
        System.out.println("Enter new user password: ");
        @NotNull final String password = bootstrap.getScanner().nextLine();
        System.out.println("Enter new user role(admin or user): ");
        bootstrap.getUserService().create(login, password, RoleType.valueOf(bootstrap.getScanner().nextLine().toUpperCase()));
        System.out.println("User created!");
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN};
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return false;
    }

}
