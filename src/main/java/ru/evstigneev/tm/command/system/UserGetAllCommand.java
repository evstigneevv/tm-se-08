package ru.evstigneev.tm.command.system;

import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;

public class UserGetAllCommand extends AbstractCommand {

    @Override
    public String command() {
        return "SAU";
    }

    @Override
    public String description() {
        return "Show all users";
    }

    @Override
    public void execute() throws Exception {
        for (User u : bootstrap.getUserService().findAll()) {
            System.out.println("login: " + u.getLogin() + " | ID: " + u.getId() + " | ROLE: " + u.getRole());
        }
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN};
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}
