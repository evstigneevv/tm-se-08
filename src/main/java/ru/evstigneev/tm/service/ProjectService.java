package ru.evstigneev.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.IProjectRepository;
import ru.evstigneev.tm.api.IProjectService;
import ru.evstigneev.tm.api.ITaskRepository;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.exception.RepositoryException;

import java.util.Collection;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    private IProjectRepository projectRepository;
    private ITaskRepository taskRepository;

    public ProjectService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public Collection<Project> findAll() throws Exception {
        if (projectRepository.findAll().isEmpty()) {
            throw new RepositoryException("No projects found!");
        }
        return projectRepository.findAll();
    }

    @Override
    public Project create(@NotNull final String userId, @NotNull final String projectName) throws EmptyStringException {
        if (projectName.isEmpty()) {
            throw new EmptyStringException();
        }
        return projectRepository.create(userId, projectName);
    }

    @Override
    public boolean remove(@NotNull final String userId, @NotNull final String projectId) throws EmptyStringException {
        if (projectId.isEmpty()) {
            throw new EmptyStringException();
        }
        taskRepository.deleteAllProjectTasks(userId, projectId);
        return projectRepository.remove(userId, projectId);
    }

    @Override
    public Project update(@NotNull final String userId, @NotNull final String projectId, @NotNull final String newProjectName) throws EmptyStringException {
        if (projectId.isEmpty() || newProjectName.isEmpty()) {
            throw new EmptyStringException();
        }
        return projectRepository.update(userId, projectId, newProjectName);
    }

    @Override
    public Project findOne(@NotNull final String userId, @NotNull final String projectId) throws EmptyStringException {
        if (projectId.isEmpty()) {
            throw new EmptyStringException();
        }
        return projectRepository.findOne(userId, projectId);
    }

    @Override
    public Project merge(@NotNull final String userId, @NotNull final Project project) throws Exception {
        return projectRepository.merge(userId, project);
    }

    @Override
    public Project persist(@NotNull final String userId, @NotNull final Project project) throws Exception {
        return projectRepository.persist(userId, project);
    }

    @Override
    public Collection<Project> findAllByUserId(@NotNull final String userId) {
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    public void removeAll() {
        projectRepository.removeAll();
    }

    @Override
    public boolean removeAllByUserId(@NotNull final String userId) {
        taskRepository.removeAllByUserId(userId);
        return projectRepository.removeAllByUserId(userId);
    }

}
