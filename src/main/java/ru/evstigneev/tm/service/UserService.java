package ru.evstigneev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.IUserRepository;
import ru.evstigneev.tm.api.IUserService;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collection;

public class UserService extends AbstractService<User> implements IUserService {

    private IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User create(@NotNull final String login, @NotNull final String password) {
        return userRepository.create(login, password);
    }

    @Override
    public User create(@NotNull final String login, @NotNull final String password, @NotNull final RoleType role) {
        return userRepository.create(login, password, role);
    }

    @Override
    public Collection<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public boolean checkPassword(@NotNull final String login, @NotNull final String password) {
        try {
            @Nullable Collection<User> users = findAll();
            if (users != null) {
                for (User u : users) {
                    if (u.getLogin().equals(login)) {
                        return Arrays.toString(MessageDigest.getInstance("MD5").digest(password.getBytes())).equals(u.getPasswordHash());
                    }
                }
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean updatePassword(@NotNull final String login, @NotNull final String newPassword) {
        return userRepository.updatePassword(login, newPassword);
    }

    @Override
    public boolean isAdmin(@NotNull final User user) {
        return user.getRole().equals(RoleType.ADMIN);
    }

    @Override
    public User findByLogin(@NotNull final String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final String userName) {
        userRepository.update(userId, userName);
    }

    @Override
    public boolean remove(@NotNull final String userId) {
        return userRepository.remove(userId);
    }

    @Override
    public User persist(@NotNull final String userId, @NotNull final User user) {
        return userRepository.persist(userId, user);
    }

    @Override
    public void removeAll() {
        userRepository.removeAll();
    }

    @Override
    public User merge(@NotNull final String userId, @NotNull final User user) {
        return userRepository.merge(userId, user);
    }

}
