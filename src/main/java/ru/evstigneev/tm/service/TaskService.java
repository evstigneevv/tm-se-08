package ru.evstigneev.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.ITaskRepository;
import ru.evstigneev.tm.api.ITaskService;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.exception.EmptyStringException;

import java.util.Collection;

public class TaskService extends AbstractService<Task> implements ITaskService {

    private ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task create(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskName) throws EmptyStringException {
        if (taskName.isEmpty() || projectId.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.create(userId, projectId, taskName);
    }

    @Override
    public Collection<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Collection<Task> findAllByUserId(@NotNull final String userId) {
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    public boolean remove(@NotNull final String userId, @NotNull final String taskId) throws EmptyStringException {
        if (taskId.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.remove(userId, taskId);
    }

    @Override
    public Collection<Task> getTaskListByProjectId(@NotNull final String userId, @NotNull final String projectId) throws EmptyStringException {
        if (projectId.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.getTaskListByProjectId(userId, projectId);
    }

    @Override
    public Task update(@NotNull final String userId, @NotNull final String taskId, @NotNull final String taskName) throws EmptyStringException {
        if (taskId.isEmpty() || taskName.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.update(userId, taskId, taskName);
    }

    @Override
    public Task findOne(@NotNull final String userId, @NotNull final String taskId) throws EmptyStringException {
        if (taskId.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.findOne(userId, taskId);
    }

    @Override
    public Task merge(@NotNull final String userId, @NotNull final Task task) throws Exception {
        return taskRepository.merge(userId, task);
    }

    @Override
    public Task persist(@NotNull final String userId, @NotNull final Task task) throws Exception {
        return taskRepository.persist(userId, task);
    }

    @Override
    public void removeAll() {
        taskRepository.removeAll();
    }

    @Override
    public boolean removeAllByUserId(@NotNull final String userId) {
        return taskRepository.removeAllByUserId(userId);
    }

    @Override
    public boolean deleteAllProjectTasks(@NotNull final String userId, @NotNull final String projectId) {
        return taskRepository.deleteAllProjectTasks(userId, projectId);
    }

}
