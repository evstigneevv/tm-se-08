package ru.evstigneev.tm.service;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;

public abstract class AbstractService<V> {

    public abstract V merge(@NotNull final String userId, @NotNull final V value) throws Exception;

    public abstract V persist(@NotNull final String userId, @NotNull final V value) throws Exception;

    public abstract void removeAll();

    public abstract Collection<V> findAll() throws Exception;

}
