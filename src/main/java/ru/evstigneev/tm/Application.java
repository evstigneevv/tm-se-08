package ru.evstigneev.tm;

import org.apache.log4j.BasicConfigurator;
import ru.evstigneev.tm.bootstrap.Bootstrap;
import ru.evstigneev.tm.command.project.*;
import ru.evstigneev.tm.command.system.*;
import ru.evstigneev.tm.command.task.*;

public class Application {

    private static final Class[] CLASSES = {HelpCommand.class, ProjectCreateCommand.class,
            ProjectDeleteCommand.class, ProjectGetAllCommand.class, ProjectUpdateCommand.class, TaskCreateCommand.class,
            TaskUpdateCommand.class, TaskDeleteCommand.class, TaskGetCommand.class, TaskGetOneCommand.class,
            TaskGetAllCommand.class, TaskDeleteAllCommand.class, ProjectDeleteAllCommand.class, ExitCommand.class,
            UserCreateCommand.class, UserLogInCommand.class, UserLogOutCommand.class, ProjectGetByUserCommand.class,
            UserGetAllCommand.class, UserUpdateCommand.class, UserShowCurrentCommand.class, TaskGetByUserCommand.class,
            UserDeleteCommand.class, AboutCommand.class, UserUpdatePasswordCommand.class};

    public static void main(String[] args) throws Exception {
        BasicConfigurator.configure();
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(CLASSES);
    }

}