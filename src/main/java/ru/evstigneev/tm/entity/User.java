package ru.evstigneev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.enumerated.RoleType;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

@Getter
@Setter
@NoArgsConstructor
public class User {

    @NotNull
    private String login;
    @NotNull
    private String id;
    @NotNull
    private String passwordHash;
    @NotNull
    private RoleType role;

    public void setPasswordHash(final String password) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            passwordHash = Arrays.toString(messageDigest.digest(password.getBytes()));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

}