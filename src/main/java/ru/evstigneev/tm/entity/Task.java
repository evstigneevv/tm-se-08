package ru.evstigneev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Task {

    @NotNull
    private String name;
    @NotNull
    private String id;
    @NotNull
    private String projectId;
    @Nullable
    private String description;
    @Nullable
    private Date dateStart;
    @Nullable
    private Date dateFinish;
    @NotNull
    private String userId;

}

