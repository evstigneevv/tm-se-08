package ru.evstigneev.tm.repository;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public abstract class AbstractRepository<V> {

    final Map<String, V> entities = new LinkedHashMap<>();

    public abstract V merge(@NotNull final String userId, @NotNull final V value);

    public abstract V persist(@NotNull final String userId, @NotNull final V value);

    public abstract void removeAll();

    public abstract Collection<V> findAll();

}
