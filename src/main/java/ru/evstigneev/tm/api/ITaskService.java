package ru.evstigneev.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.exception.EmptyStringException;

import java.util.Collection;

public interface ITaskService {

    Task create(@NotNull final String userId, @NotNull final String taskName, @NotNull final String projectId) throws EmptyStringException;

    Collection<Task> findAll();

    Collection<Task> findAllByUserId(@NotNull final String userId);

    boolean remove(@NotNull final String userId, @NotNull final String taskId) throws EmptyStringException;

    Collection<Task> getTaskListByProjectId(@NotNull final String userId, @NotNull final String projectId) throws EmptyStringException;

    Task update(@NotNull final String userId, @NotNull final String taskId, @NotNull final String taskName) throws EmptyStringException;

    Task findOne(@NotNull final String userId, @NotNull final String taskId) throws EmptyStringException;

    Task merge(@NotNull final String userId, @NotNull final Task task) throws Exception;

    Task persist(@NotNull final String userId, @NotNull final Task task) throws Exception;

    void removeAll();

    boolean removeAllByUserId(@NotNull final String userId);

    boolean deleteAllProjectTasks(@NotNull final String userId, @NotNull final String projectId);

}
