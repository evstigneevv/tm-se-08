package ru.evstigneev.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.exception.EmptyStringException;

import java.util.Collection;

public interface IProjectRepository {

    Project create(@NotNull final String userId, @NotNull final String projectName) throws EmptyStringException;

    boolean remove(@NotNull final String userId, @NotNull final String projectId) throws EmptyStringException;

    Project update(@NotNull final String userId, @NotNull final String projectId, @NotNull final String projectName) throws EmptyStringException;

    Collection<Project> findAll() throws Exception;

    Collection<Project> findAllByUserId(@NotNull final String userId);

    Project findOne(@NotNull final String userId, @NotNull final String projectId) throws EmptyStringException;

    Project merge(@NotNull final String userId, @NotNull final Project project) throws Exception;

    Project persist(@NotNull final String userId, @NotNull final Project project) throws Exception;

    boolean removeAllByUserId(@NotNull final String userId);

    void removeAll();

}
