package ru.evstigneev.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.exception.EmptyStringException;

import java.util.Collection;

public interface IProjectService {

    Collection<Project> findAll() throws Exception;

    Project create(@NotNull final String userId, @NotNull final String projectName) throws EmptyStringException;

    boolean remove(@NotNull final String userId, @NotNull final String projectId) throws EmptyStringException;

    Project update(@NotNull final String userId, @NotNull final String projectId, @NotNull final String newProjectName) throws EmptyStringException;

    Project findOne(@NotNull final String userId, @NotNull final String projectId) throws EmptyStringException;

    Project merge(@NotNull final String userId, @NotNull final Project project) throws Exception;

    Project persist(@NotNull final String userId, @NotNull final Project project) throws Exception;

    Collection<Project> findAllByUserId(@NotNull final String userId) throws Exception;

    void removeAll();

    boolean removeAllByUserId(@NotNull final String userId) throws Exception;

}
